package conRun

import (
	"context"
	"sync"
	"time"
)

// Creates a new conRun "object"
// Params:
// - ctx context.Context: context to used for canceling
// - wg *sync.WaitGroup: wait sync for safe exiting
func New() conRun {

	ctx, cancel := context.WithCancel(context.Background())

	return conRun{
		ctx:    ctx,
		cancel: cancel,
		wg:     sync.WaitGroup{},
	}
}

// adds another concurrent go function to conRun. Blocking functions recomendid
// params
// - n string: the name of the "rgo function"
// - i func(): function if anything needs to initialized
// - r func(): the function that is part of  an infinite loop that should be run over and over
// - c func(): the function that cleans up everything that we have done
func (s *conRun) AddBlocking(n string, r func()) {
	go func() {
		s.wg.Add(1)
		defer s.wg.Done()

		for {
			select {
			case <-s.ctx.Done():
				return
			default:
				r()
			}

		}

	}()
}

// adds another concurrent go function to conRun
// and runs it based on a timer
// params
// - n string: the name of the "go function"
// - t time.Duration: interval between running the r function
// - i func(): function if anything needs to initialized
// - r func(): the function that is part of  an infinite loop that should be run over and over
// - c func(): the function that cleans up everything that we have done
func (s *conRun) AddTimed(n string, t time.Duration, r func()) {
	go func() {
		s.wg.Add(1)
		defer s.wg.Done()

		r()

		ticker := time.NewTicker(t)

		for {
			select {
			default:
				//time.Sleep(30 * time.Second)
				time.Sleep(1 * time.Millisecond)
			case <-ticker.C:
				r()
			case <-s.ctx.Done():
				return
			}
		}

	}()
}

// adds another concurrent go function to conRun
// this is used for channel handlers (only one handler per channel)
// params
// - ch chan string: this is the chanel we will be handling
// - f func(string): this is the functions that will be handling the data from the string
func (s *conRun) AddChannelHandler(ch chan string, f func(string)) {
	go func() {
		s.wg.Add(1)
		defer s.wg.Done()

		for {
			select {
			default:
				time.Sleep(1 * time.Millisecond)
			case msg := <-ch:
				f(msg)
			case <-s.ctx.Done():
				// draining the channel before we exit
				for len(ch) > 0 {
					f(<-ch)
				}
				return
			}
		}

	}()
}

// this function cancels all the contexts and does a wait sync
func (s *conRun) End() {
	s.cancel()
	s.wg.Wait()
}

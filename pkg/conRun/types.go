package conRun

import (
	"context"
	"sync"
)

type conRun struct {
	ctx    context.Context
	wg     sync.WaitGroup
	cancel context.CancelFunc
}

package config

import (
	"gopkg.in/yaml.v3"
	"os"
)

// This function loads a yaml config file to a interface or struct
// Params
//   configFile:the file location
//   conf:the interface or struct
func Load(configFile string, conf interface{}) error {

	c, err := os.ReadFile(configFile)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(c, conf)
	if err != nil {
		return err
	}

	return nil
}

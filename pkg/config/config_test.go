package config

import (
	"testing"
)

type config struct {
	Foo string
}

// test loading ing and marshaling of config
func TestLoad(t *testing.T) {
	var conf = config{}

	err := Load("config_test.yaml", &conf)

	if err != nil {
		t.Errorf(err.Error())
	}

	if conf.Foo != "bar" {
		t.Errorf("config not loaded")

	}

	err = Load("moo", &conf)

	if err == nil {
		t.Errorf("sould have returned err")
	}

}

# sys-starter

Basic examples of how to implement concurrent safe systems with safe exits
There are two different examples:
 - **simple**: is a basic implementation of two known concurrent routines.
 - **lib**: spawns concurrent blocking timers and makes use of a internal lib to control these


## running 

### simple

```bash
make simple
```

### lib

```bash
make lib
```
